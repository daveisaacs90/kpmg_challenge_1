variable "project_id" {
  type = string
  default = "three-t-env"
}

variable "project_number" {
  type = string
  default = "40025212434"
}

variable "region" {
  type = string
  default = "us-west1"
}

variable "zone" {
  type = string
  default = "us-west1-a"
}

variable "basename" {
  type = string
  default = "three-tier"
}

locals {
  sabuild = "${var.project_number}@cloudbuild.gserviceaccount.com"
}

# Handle services
variable "gcp_service_list" {
  description = "The list of apis necessary for the project"
  type        = list(string)
  default = [
    "compute.googleapis.com",
    "cloudapis.googleapis.com",
    "vpcaccess.googleapis.com",
    "servicenetworking.googleapis.com",
    "cloudbuild.googleapis.com",
    "sql-component.googleapis.com",
    "sqladmin.googleapis.com",
    "storage.googleapis.com",
    "secretmanager.googleapis.com",
    "run.googleapis.com",
    "artifactregistry.googleapis.com",
    "redis.googleapis.com"
  ]
}
